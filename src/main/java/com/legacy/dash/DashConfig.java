package com.legacy.dash;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;

public class DashConfig
{
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ServerConfig SERVER;

	public static boolean useDurability()
	{
		return SERVER.useDurability.get();
	}
	static
	{
		{
			final Pair<ServerConfig, ForgeConfigSpec> serverPair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = serverPair.getLeft();
			SERVER_SPEC = serverPair.getRight();
		}
	}

	private static class ServerConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> useDurability;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Dash Server/World Config");
			
			builder.push("Enchantment");
			useDurability = builder.comment("\n Cause damage to boots when dashing. (Affected by Unbreaking)").define("useDurability", false);
			builder.pop();
		}
	}
}
