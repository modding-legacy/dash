package com.legacy.dash.network.c_to_s;

import java.util.function.Supplier;

import com.legacy.dash.DashConfig;
import com.legacy.dash.DashRegistry;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraftforge.network.NetworkEvent;

public class DashVisualsPacket
{
	public DashVisualsPacket()
	{
	}

	public static void encoder(DashVisualsPacket packet, FriendlyByteBuf buff)
	{
	}

	public static DashVisualsPacket decoder(FriendlyByteBuf buff)
	{
		return new DashVisualsPacket();
	}

	public static void handler(DashVisualsPacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> handlePacket(packet, context.get().getSender()));
		context.get().setPacketHandled(true);
	}

	private static void handlePacket(DashVisualsPacket packet, ServerPlayer player)
	{
		ServerLevel lvl = player.getLevel();

		var width = player.getBbWidth() / 2;
		var height = player.getBbHeight() * 0.3F;
		lvl.sendParticles(ParticleTypes.POOF, player.getX(), player.getY() + height, player.getZ(), 30, width, height, width, 0.08F);
		lvl.sendParticles(ParticleTypes.ENCHANTED_HIT, player.getX(), player.getY() + height, player.getZ(), 10, width, height, width, 0.2F);

		lvl.playSound(null, player, DashRegistry.DASH_SOUND.get(), SoundSource.PLAYERS, 1.0F, 2.0F);

		if (DashConfig.useDurability())
			player.getItemBySlot(EquipmentSlot.FEET).hurtAndBreak(1, player, (living) -> player.broadcastBreakEvent(EquipmentSlot.FEET));
	}
}
