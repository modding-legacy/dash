package com.legacy.dash;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantment.Rarity;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

@EventBusSubscriber(modid = DashMod.MODID, bus = Bus.MOD)
public class DashRegistry
{
	private static final ResourceLocation DASH_KEY = DashMod.locate("entity.player.ench_dash");

	public static final Lazy<Enchantment> DASHING = Lazy.of(() -> new DashingEnchantment(Rarity.RARE, EquipmentSlot.FEET));
	public static final Lazy<SoundEvent> DASH_SOUND = Lazy.of(() -> SoundEvent.createVariableRangeEvent(DASH_KEY));

	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(ForgeRegistries.Keys.ENCHANTMENTS))
			event.register(ForgeRegistries.ENCHANTMENTS.getRegistryKey(), DashMod.locate("dashing"), DASHING);
		else if (event.getRegistryKey().equals(ForgeRegistries.Keys.SOUND_EVENTS))
			event.register(ForgeRegistries.SOUND_EVENTS.getRegistryKey(), DASH_KEY, DASH_SOUND);
	}
}
