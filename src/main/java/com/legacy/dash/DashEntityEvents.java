package com.legacy.dash;

import com.legacy.dash.network.PacketHandler;
import com.legacy.dash.network.c_to_s.DashVisualsPacket;

import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class DashEntityEvents
{
	byte cooldown = 0;

	@SubscribeEvent
	public void onKeyPressed(InputEvent.Key event)
	{
		Minecraft mc = Minecraft.getInstance();

		if (mc.player == null)
			return;

		Player player = mc.player;

		float enchantmentLevel = EnchantmentHelper.getEnchantmentLevel(DashRegistry.DASHING.get(), player);

		if (DashMod.DashClient.DASH_KEYBIND.isDown() && cooldown <= 0 && enchantmentLevel > 0 && player.zza != 0 && !player.isInWaterOrBubble() && player.getFallFlyingTicks() <= 10)
		{
			PacketHandler.sendToServer(new DashVisualsPacket());

			Vec3 playerLook = player.getViewVector(1);
			Vec3 dashVec = new Vec3(playerLook.x(), player.getDeltaMovement().y(), playerLook.z());
			player.setDeltaMovement(dashVec);

			cooldown = 50;
		}
	}

	@SubscribeEvent
	public void onTick(TickEvent.PlayerTickEvent event)
	{
		if (cooldown > 0 && event.phase.equals(Phase.START) && event.side.isClient() && event.player.equals(Minecraft.getInstance().player))
			--cooldown;
	}
}
